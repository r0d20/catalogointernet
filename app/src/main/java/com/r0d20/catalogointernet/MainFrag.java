package com.r0d20.catalogointernet;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.animation.AnimatorCompatHelper;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.net.ConnectivityManagerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFrag extends Fragment implements ItemAdapter.HolderClickListener{

    RecyclerView rv;
    TextView tv_no_internet;
    ItemAdapter adapter;
    ItemAdapter.HolderClickListener listener;
    static List<ItemModel> resultados;

    static final String TAG = "MainFrag";

    public MainFrag() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setInterpolator(new AccelerateDecelerateInterpolator());
            setEnterTransition(fade);
            setExitTransition(fade);
            /*Slide slide = new Slide();
            slide.setSlideEdge(Gravity.RIGHT);
            setEnterTransition(slide);
            slide.setSlideEdge(Gravity.LEFT);
            setExitTransition(slide);*/
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv = (RecyclerView) view.findViewById(R.id.recyclerview);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        //rv.setLayoutManager(new GridLayoutManager(getContext(),2));

        listener = this;
        tv_no_internet = (TextView) view.findViewById(R.id.txt_no_internet);
    }

    void dialogSinConexion(){
        if(resultados == null)
            tv_no_internet.setVisibility(View.VISIBLE);

        final BottomSheetDialog dialog = new BottomSheetDialog(getContext());
        dialog.setContentView(R.layout.fragment_sin_internet_btm_sheet);
        TextView tv_hab_internet = (TextView) dialog.findViewById(R.id.tv_hab_internet);
        tv_hab_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), 0);
                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                dialog.dismiss();
            }
        });

        TextView tv_no_hab_internet = (TextView) dialog.findViewById(R.id.tv_no_hab_internet);
        tv_no_hab_internet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        //Toast.makeText(getContext(),"dialog sin conexion", Toast.LENGTH_SHORT).show();
    }

    void Llenar_Adapter() {
        if (resultados != null) {
            Log.d(TAG, "cache resultados");
            adapter = new ItemAdapter(getContext(), resultados, listener);
            rv.setAdapter(adapter);
        } else
            GetData();

        tv_no_internet.setVisibility(View.GONE);
    }

    private void GetData(){
        final ProgressDialog loadingDialog = ProgressDialog.show(getContext(),"Cargando..","",false,false);

        //url orig "http://10.0.2.2/lugares.php"
        StringRequest request = new StringRequest(BuildConfig.SERVER_URL+"/lugares.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loadingDialog.dismiss();

                        resultados = new Gson().fromJson(response, new TypeToken<List<ItemModel>>(){}.getType());
                        adapter = new ItemAdapter(getContext(),resultados, listener);
                        rv.setAdapter(adapter);
//                        Toast.makeText(getContext(),"conexion hecha", Toast.LENGTH_SHORT).show();
                        Log.d(TAG,"ok response");
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        loadingDialog.dismiss();
                        Log.d(TAG,"error response de "+BuildConfig.SERVER_URL);
                        Toast.makeText(getContext(),"error en conexion", Toast.LENGTH_SHORT).show();
                    }
                }
        );

        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(request);
    }

    @Override
    public void onHolderClicked(ItemAdapter.ItemHolder holder, String urlFoto) {

                //String transicion_nombre = "transicion_detalle"+holder.getAdapterPosition();
                //String transicion_nombre = "transicion";

                DetalleFrag frag = DetalleFrag.newInstance(
                        holder.titulo.getText().toString(), holder.descripcion.getText().toString(), urlFoto);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //transition
                    TransitionSet transitionSet = new TransitionSet();
                    transitionSet.addTransition(new ChangeBounds());
                    transitionSet.addTransition(new ChangeTransform());
                    transitionSet.setInterpolator(new AccelerateDecelerateInterpolator());
                    //transitionSet.addTransition(new ChangeImageTransform());

                    //frag shared
                    frag.setSharedElementEnterTransition(transitionSet);
                    frag.setEnterTransition(new ChangeTransform());
                    frag.setExitTransition(new ChangeTransform());
                    frag.setSharedElementReturnTransition(transitionSet);

                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .addSharedElement(holder.itemView,"transicion")
                            .addSharedElement(holder.foto,"transicion_foto")
                            .replace(R.id.container, frag)
                            .addToBackStack(TAG).commit();
                    Log.d(TAG, "frag shared element transition");
                }
                else {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .replace(R.id.container, frag)
                            .addToBackStack(TAG).commit();
                    Log.d(TAG, "frag sin transition");
                }


    }
}
