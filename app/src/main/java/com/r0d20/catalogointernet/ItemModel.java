package com.r0d20.catalogointernet;

import android.content.ClipData;

/**
 * Created by Rodrigo Gutierrez on 04/01/2017.
 */

public class ItemModel {
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFoto_path(String foto_path) {
        this.foto_path = foto_path;
    }

    String titulo;
    String descripcion;
    String foto_path;

    public ItemModel(){}

    public ItemModel(String foto_path, String titulo, String descripcion) {
        this.foto_path = foto_path;
        this.titulo = titulo;
        this.descripcion = descripcion;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getFoto_path() {
        return foto_path;
    }

}
