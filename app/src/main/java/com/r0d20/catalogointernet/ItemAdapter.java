package com.r0d20.catalogointernet;

import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemHolder>  {

    private static final String TAG = "ItemAdapter";

    Context context;
    List<ItemModel> items = new ArrayList<>();

    HolderClickListener listener;

    public ItemAdapter(Context context, List<ItemModel> items, HolderClickListener listParam) {
        this.items = items;
        this.context = context;
        listener = listParam;
    }

    @Override
    public ItemHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        final View v = LayoutInflater.from(context).inflate(R.layout.item_detalle, parent, false);
        final ItemHolder holder = new ItemHolder(v);

        Log.d(TAG,"onCreateViewHolder");
        return holder;
    }

    @Override
    public void onBindViewHolder(final ItemHolder holder, int position) {

        final ItemModel itemLugar = items.get(position);
        holder.titulo.setText(itemLugar.titulo);
        holder.descripcion.setText(itemLugar.descripcion);

        ViewCompat.setTransitionName(holder.foto, "transicion_foto"+String.valueOf(holder.getAdapterPosition()));
        ViewCompat.setTransitionName(holder.itemView, "transicion"+String.valueOf(holder.getAdapterPosition()));
        Log.d(TAG, "transicion"+String.valueOf(holder.getAdapterPosition()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onHolderClicked(holder, itemLugar.foto_path);
            }
        });


        if(itemLugar.foto_path != null)
            Glide.with(context).load(itemLugar.foto_path).centerCrop().into(holder.foto);

//        Glide.with(context).load(R.drawable.foto_default).into(holder.foto);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface HolderClickListener{
        void onHolderClicked(ItemHolder holder, String urlFoto);
    }

    class ItemHolder extends RecyclerView.ViewHolder{
        TextView titulo;
        TextView descripcion;
        ImageView foto;

        ItemHolder(View view){
            super(view);

            titulo = (TextView)view.findViewById(R.id.titulo);
            descripcion = (TextView)view.findViewById(R.id.descripcion);
            foto = (ImageView) view.findViewById(R.id.imagen);
        }
    }
}

