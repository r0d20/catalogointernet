package com.r0d20.catalogointernet;


import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetalleFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleFrag extends Fragment {
    private static final String ARG_TITULO = "mTitulo";
    private static final String ARG_DESCRIPCION = "mDescripcion";
    private static final String ARG_FOTO_URL = "ARG_FOTO_URL";

    private static final String TAG = "DetalleFrag";

    private String titulo;
    private String descripcion;
    private String foto_path;

    TextView tv_titulo, tv_descripcion;
    ImageView iv_foto;

    public DetalleFrag() {
        // Required empty public constructor
    }

    public static DetalleFrag newInstance(String param1, String param2, String urlFoto) {
        DetalleFrag fragment = new DetalleFrag();
        Bundle args = new Bundle();
        args.putString(ARG_TITULO, param1);
        args.putString(ARG_DESCRIPCION, param2);
        args.putString(ARG_FOTO_URL, urlFoto);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            titulo = getArguments().getString(ARG_TITULO);
            descripcion = getArguments().getString(ARG_DESCRIPCION);
            foto_path = getArguments().getString(ARG_FOTO_URL);
            Log.d(TAG,"onCreate, titulo: "+ titulo + " descripcion: "+descripcion);
        }
        else
            Log.d(TAG,"onCreate, args == null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lugares, container, false);
        iv_foto = (ImageView)view.findViewById(R.id.imagen);
        tv_titulo = (TextView)view.findViewById(R.id.titulo);
        tv_descripcion = (TextView)view.findViewById(R.id.descripcion);

        tv_titulo.setText(titulo);
        tv_descripcion.setText(descripcion);

        Glide.with(this).load(foto_path).skipMemoryCache(false).dontAnimate().into(iv_foto);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewCompat.setTransitionName(view, "transicion");
            ViewCompat.setTransitionName(iv_foto, "transici on_foto");

            //ViewCompat.setTransitionName(iv_foto, getArguments().getString(ARG_ID_HOLDER));
            //iv_foto.setTransitionName(getArguments().getString(ARG_ID_HOLDER));

            //Log.d(TAG, "transition Name "+iv_foto.getTransitionName());
        }
    }

}
