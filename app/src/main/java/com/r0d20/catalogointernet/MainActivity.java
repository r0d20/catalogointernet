package com.r0d20.catalogointernet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    private BroadcastReceiver estadoInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View v = View.inflate(this, R.layout.activity_main, null);
        setContentView(v);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new MainFrag(), "mainFrag")
                    .commit();
        }

        estadoInternet = new BroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {

                if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){
                    ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = connMgr.getActiveNetworkInfo();
                    boolean conectado = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                    final MainFrag frag = (MainFrag) getSupportFragmentManager().findFragmentByTag("mainFrag");
                    if(conectado) {
                        Runnable runnable = new Runnable() {
                            @Override
                            public void run() {
                                if (frag != null)
                                    frag.Llenar_Adapter();
                            }};
                        runnable.run();
                    }
                    else frag.dialogSinConexion();
                }
            }
        };
        registerReceiver(estadoInternet, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(estadoInternet);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if(getSupportFragmentManager().getBackStackEntryCount() > 0)
            getSupportFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }



}
